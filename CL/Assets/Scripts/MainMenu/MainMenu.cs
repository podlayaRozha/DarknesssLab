﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    public void OpenMainMenu()
    {
        Animator animator = GameObject.Find("Door").GetComponent<Animator>();

        animator.SetBool("StartOpen", true);

        this.gameObject.active = false;
    }
}
